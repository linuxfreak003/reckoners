package reckoners

import (
	"math/rand"
)

// PlayerMove is a move that can be rolled
type PlayerMove int

// All the different die options
const (
	ContainEpic PlayerMove = iota
	ResearchEpic
	AttackEpic
	AttackEnforcement
	Plan
	Money
	DoubleContainEpic
	DoubleResearchEpic
	DoubleAttackEpic
	DoubleAttackEnforcement
	DoublePlan
	DoubleMoney
)

// Die is a die
type Die interface {
	Roll() PlayerMove
}

// WhiteDie is the most basic die
type WhiteDie struct{}

// Roll ...
func (WhiteDie) Roll() PlayerMove {
	return PlayerMove(rand.Intn(6))
}

// GrayDie is the die used by prof
type GrayDie struct{}

// Roll ...
func (GrayDie) Roll() PlayerMove {
	m := PlayerMove(rand.Intn(6))
	if m == Plan {
		return DoublePlan
	}
	return m
}
