// Package reckoners is a go library for reckoners board game
package reckoners

import (
	"fmt"
	"time"
)

type Equipment struct{}

type Player struct {
	Dice           []Die       `json:"dice"`
	EquipmentCards []Equipment `json:"equipmentCards"`
	PlanTokens     int         `json:"planTokens"`
}

type City struct {
	Epic     Epic `json:"epic"`
	Research int  `json:"research"`
	Health   int  `json:"health"`
}

type Epic struct {
	Research int `json:"research"`
	Health   int `json:"health"`
	// EpicAttack
}

type Board struct {
	Players     []Player    `json:"players"`
	Cities      []City      `json:"cities"`
	PlayerBoard PlayerBoard `json:"playerBoard"`
}

type PlayerBoard struct {
	Money         int         `json:"money"`
	Hideout       int         `json:"hideout"`
	Population    int         `json:"population"`
	EquipmentDeck []Equipment `json:"equipmentDeck"`
	OptionOne     Equipment   `json:"optionOne"`
	OptionTwo     Equipment   `json:"optionTwo"`
	OptionThree   Equipment   `json:"optionThree"`
	OptionFour    Equipment   `json:"optionFour"`
}

// Log logs
func Log() {
	fmt.Println(time.Now())
}
