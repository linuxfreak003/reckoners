package main

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/linuxfreak003/reckoners"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	d := reckoners.GrayDie{}
	for i := 0; i < 10; i++ {
		fmt.Println(d.Roll())
	}
}
